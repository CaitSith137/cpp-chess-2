# CPP Chess

Chess with all the rules implemented in C++ as a programming exercise. My first game dev project. The code is horrible!

Moves are entered in algebraic chess notation. Ambiguous moves must be entered with rank and file, e.g. Rf6h6. Castling with letter O, e.g. O-O.  

To do:

-Debug check detection (blocking)  
-Optimize, cleanup  
-Timer  
-Save game  
-User undo move  
-Repetition detection  
-Simple eval bar  
-Bonus: simple chess engine  