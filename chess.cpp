#include <iostream>
#include <fstream>
#include <string>

#include "board.hpp"
#include "utility.hpp"

int main() {

  Board board;
  board.init();
  MoveList m;
  
  std::string input;
  int round = 1;
  int move = 1;

  std::cout << "Enter 0 to load game, enter 1 to play\n";
  std::cin >> input;
  std::string filename;
  if (input == "0") {
    std::cout << "Loading game.\nEnter 0 to load test game, enter 1 to load other game.\n";
    std::cin >> input;
    if (input == "0") filename = "testgame.txt";
    else {
      std::cout << "Enter filename of saved game:\n";
      std::cin >> input;
      filename = input;
    }
  }
  std::ifstream file(filename);

  while (true) {
    std::getline(file, input);
		 
    board.WhiteMove = round % 2 > 0;
    
    std::cout << std::endl;
    board.print();
    std::cout << std::endl;
    
    std::cout << "MOVE NUMBER " << move << ": ";
    
    if (board.WhiteMove) {
      std::cout << "WHITE TO MOVE" << std:: endl;
      if (board.WhiteInCheck)
	std::cout << "WHITE IN CHECK" << std::endl;
    }
    else {
      std::cout << "BLACK TO MOVE" << std:: endl;
      if (board.BlackInCheck)
	std::cout << "BLACK IN CHECK" << std::endl;
    }

    if (!file) std::cin >> input;
    else std::cout << input << std::endl;

    std::cin.get();

    if (input=="P")
      {m.PrintMoveList(); board.legalMove = false;}
    else
      board.move(input);

    if (board.legalMove) {
      round++;
      move = round%2==0 ? round/2 : (round+1)/2;
      m.MoveList.push_back(input);
    }

    if (board.Checkmate) {
      if (board.WhiteMove) std::cout << "White is victorious!\n";
      else std::cout << "Black is victorious!\n";
      break;
    }
    else if (board.Stalemate) {
      std::cout << "Stalemate!\n";
      break;
    }
  }

  std::cout << std::endl;
  board.print();
  std::cin.get();
}


