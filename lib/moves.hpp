#ifndef MOVES_HPP
#define MOVES_HPP

#include <iostream>
#include "board.hpp"

bool PawnMove(bool colorLogic, Board& b, int i, int j, int& Rank, int& File);

bool PawnCapture(bool colorLogic, Board& b, int i, int j, int& Rank, int& File);

bool KnightMove(bool colorLogic, Board& b, int i, int j, int& Rank, int& File);

bool BishopMove (bool colorLogic, Board& b, int i, int j, int& Rank, int& File);

bool RookMove (bool colorLogic, Board& b, int i, int j, int& Rank, int& File);

bool QueenMove (bool colorLogic, Board& b, int i, int j, int& Rank, int& File);

bool KingMove (bool colorLogic, Board& b, int i, int j, int& Rank, int& File);

#endif
