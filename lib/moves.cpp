#include "moves.hpp"


bool PawnCapture(bool colorLogic, Board& b, int i, int j, int& Rank, int& File) {
  
  if (colorLogic && j-1 > -1 && i+1 < 8 && b.board_[i+1][j-1] == "♙" && !b.isEmpty(i,j)) {
    Rank = i+1; File = j-1; return true;
  }
  else if (colorLogic && j+1 < 8 && i+1 < 8 && b.board_[i+1][j+1] == "♙" && !b.isEmpty(i,j)) {
    Rank = i+1; File = j+1; return true;
  }
  
  else if (!colorLogic && j-1 > -1 && i-1 > -1 && b.board_[i-1][j-1] == "♟" && !b.isEmpty(i,j)) {
    Rank = i-1; File = j-1; return true;
  }
  else if (!colorLogic && j+1 < 8 && i-1 > -1 && b.board_[i-1][j+1] == "♟" && !b.isEmpty(i,j)) {
    Rank = i-1; File = j+1; return true;
  }
  
  else if (colorLogic && j-1 > -1 && i+1 < 8 && b.board_[i+1][j-1] == "♙" && b.WhiteEnPassant && b.isEmpty(i,j) && b.EnPassantFile == j) {b.EnPassant = true; return true;}
  else if (colorLogic && j+1 < 8 && i+1 < 8 && b.board_[i+1][j+1] == "♙" && b.WhiteEnPassant && b.isEmpty(i,j) && b.EnPassantFile == j) {b.EnPassant = true; return true;} 
  else if (!colorLogic && j-1 > -1 && i-1 > -1 && b.board_[i-1][j-1] == "♟" && b.BlackEnPassant && b.isEmpty(i,j) && b.EnPassantFile == j) {b.EnPassant = true; return true;}
  else if (!colorLogic && j+1 < 8 && i-1 > -1 && b.board_[i-1][j+1] == "♟" && b.BlackEnPassant && b.isEmpty(i,j) && b.EnPassantFile == j) {b.EnPassant = true; return true;}

  return false;
}

bool PawnMove(bool colorLogic, Board& b, int i, int j, int& Rank, int& File) {

  if (colorLogic && b.board_[i+1][j] == "♙" && b.isEmpty(i,j)) {Rank = i+1; return true;}
  else if (colorLogic && i == 4 && b.board_[6][j] == "♙" && b.isEmpty(5,j)) {Rank = 6; return true;}

  else if (!colorLogic && b.board_[i-1][j] == "♟" && b.isEmpty(i,j)) {Rank = i-1; return true;}
  else if (!colorLogic && i == 3 && b.board_[1][j] == "♟" && b.isEmpty(2,j)) {Rank = 1; return true;}

  return false;
}

bool KnightMove(bool colorLogic, Board& b, int i, int j, int& Rank, int& File) {

  if (i+1 < 8 && j+2 < 8 && ((!colorLogic && b.board_[i+1][j+2] == "♞") || (colorLogic && b.board_[i+1][j+2] == "♘"))) {
    Rank = i+1; File = j+2; return true;
  }
  else if (i+1 < 8 && j-2 > -1 && ((!colorLogic && b.board_[i+1][j-2] == "♞") || (colorLogic && b.board_[i+1][j-2] == "♘"))) {
    Rank = i+1; File = j-2; return true;
  } 
  else if (i+2 < 8 && j+1 < 8 && ((!colorLogic && b.board_[i+2][j+1] == "♞") || (colorLogic && b.board_[i+2][j+1] == "♘"))) {
    Rank = i+2; File = j+1; return true;
  } 
  else if (i+2 < 8 && j-1 > -1 && ((!colorLogic && b.board_[i+2][j-1] == "♞") || (colorLogic && b.board_[i+2][j-1] == "♘"))) {
    Rank = i+2; File = j-1; return true;
  }
  else if (i-1 > -1 && j+2 < 8 && ((!colorLogic && b.board_[i-1][j+2] == "♞") || (colorLogic && b.board_[i-1][j+2] == "♘"))) {
    Rank = i-1; File = j+2; return true;
  }
  else if (i-1 > -1 && j-2 > -1 && ((!colorLogic && b.board_[i-1][j-2] == "♞") || (colorLogic && b.board_[i-1][j-2] == "♘"))) {
    Rank = i-1; File = j-2; return true;
  }
  else if (i-2 > -1 && j+1 < 8 && ((!colorLogic && b.board_[i-2][j+1] == "♞") || (colorLogic && b.board_[i-2][j+1] == "♘"))) {
    Rank = i-2; File = j+1; return true;
  }
  else if (i-2 > -1 && j-1 > -1 && ((!colorLogic && b.board_[i-2][j-1] == "♞") || (colorLogic && b.board_[i-2][j-1] == "♘"))) {
    Rank = i-2; File = j-1; return true;
  }
  return false;
}

bool BishopMove (bool colorLogic, Board& b, int i, int j, int& Rank, int& File) {
  int t = j;

  // Check diagonal top right
  for (int k = i-1; k > -1 && ++t < 8; k--) {
    if ((!colorLogic && b.board_[k][t] == "♝") || (colorLogic && b.board_[k][t] == "♗")) {
      File = t; Rank = k; return true;
    }
    else if (!b.isEmpty(k,t)) break;
  } t = j;
  // Check diagonal top left
  for (int k = i-1; k > -1 && --t > -1; k--) {
    if ((!colorLogic && b.board_[k][t] == "♝") || (colorLogic && b.board_[k][t] == "♗")) {
      File = t; Rank = k; return true;
    }
    else if (!b.isEmpty(k,t)) break;
  } t = j;
  // Check diagonal bottom right
  for (int k = i+1; k < 8 && ++t < 8; k++) {
    if ((!colorLogic && b.board_[k][t] == "♝") || (colorLogic && b.board_[k][t] == "♗")) {
      File = t; Rank = k; return true;
    }
    else if (!b.isEmpty(k,t)) break;
  } t = j;
  // Check diagonal bottom left
  for (int k = i+1; k < 8 && --t > -1; k++) {
    if ((!colorLogic && b.board_[k][t] == "♝") || (colorLogic && b.board_[k][t] == "♗")) {
      File = t; Rank = k; return true;
    }
    else if (!b.isEmpty(k,t)) break;
  }
  return false;
}

bool RookMove (bool colorLogic, Board& b, int i, int j, int& Rank, int& File) {

  // Check vertical down
  for (int k = i+1; k < 8; k++) {
    if ((!colorLogic && b.board_[k][j] == "♜") || (colorLogic && b.board_[k][j] == "♖")) {
      File = j; Rank = k; return true;
    }
    else if (!b.isEmpty(k,j)) break;
  }
  // Check vertical up
  for (int k = i-1; k > -1; k--) {
    if ((!colorLogic && b.board_[k][j] == "♜") || (colorLogic && b.board_[k][j] == "♖")) {
      File = j; Rank = k; return true;
    }
    else if (!b.isEmpty(k,j)) break;
  }
  // Check horizontal right
  for (int k = j+1; k < 8; k++) {
    if ((!colorLogic && b.board_[i][k] == "♜") || (colorLogic && b.board_[i][k] == "♖")) {
      File = k; Rank = i; return true;
    }
    else if (!b.isEmpty(i,k)) break;
  }
  // Check horizontal left
  for (int k = j-1; k > -1; k--) {
    if ((!colorLogic && b.board_[i][k] == "♜") || (colorLogic && b.board_[i][k] == "♖")) {
      File = k; Rank = i; return true;
    }
    else if (!b.isEmpty(i,k)) break;
  }
  return false;
}

bool QueenMove (bool colorLogic, Board& b, int i, int j, int& Rank, int& File) {

  int t = j;
   
    // Check vertical down
    for (int k = i+1; k < 8; k++) {
      if ((!colorLogic && b.board_[k][j] == "♛") || (colorLogic && b.board_[k][j] == "♕")) {
	File = j; Rank = k; return true;
      }
      else if (!b.isEmpty(k,j)) break;
    }
    // Check vertical up
    for (int k = i-1; k > -1; k--) {
      if ((!colorLogic && b.board_[k][j] == "♛") || (colorLogic && b.board_[k][j] == "♕")) {
	File = j; Rank = k; return true;
      }
      else if (!b.isEmpty(k,j)) break;
    }
    // Check horizontal right
    for (int k = j+1; k < 8; k++) {
      if ((!colorLogic && b.board_[i][k] == "♛") || (colorLogic && b.board_[i][k] == "♕")) {
	File = k; Rank = i; return true;
      }
      else if (!b.isEmpty(i,k)) break;
    }
    // Check horizontal left
    for (int k = j-1; k > -1; k--) {
      if ((!colorLogic && b.board_[i][k] == "♛") || (colorLogic && b.board_[i][k] == "♕")) {
	File = k; Rank = i; return true;
      }
      else if (!b.isEmpty(i,k)) break;
    }
    // Check diagonal top right
    for (int k = i-1; k > -1 && ++t < 8; k--) {
      if ((!colorLogic && b.board_[k][t] == "♛") || (colorLogic && b.board_[k][t] == "♕")) {
	File = t; Rank = k; return true;
      }
      else if (!b.isEmpty(k,t)) break;
    } t = j;
    // Check diagonal top left
    for (int k = i-1; k > -1 && --t > -1; k--) {
      if ((!colorLogic && b.board_[k][t] == "♛") || (colorLogic && b.board_[k][t] == "♕")) {
	File = t; Rank = k; return true;
      }
      else if (!b.isEmpty(k,t)) break;
    } t = j;
    // Check diagonal bottom right
    for (int k = i+1; k < 8 && ++t < 8; k++) {
      if ((!colorLogic && b.board_[k][t] == "♛") || (colorLogic && b.board_[k][t] == "♕")) {
	File = t; Rank = k; return true;
      }
      else if (!b.isEmpty(k,t)) break;
    } t = j;
    // Check diagonal bottom left
    for (int k = i+1; k < 8 && --t > -1; k++) {
      if ((!colorLogic && b.board_[k][t] == "♛") || (colorLogic && b.board_[k][t] == "♕")) {
	File = t; Rank = k; return true;
      }
      else if (!b.isEmpty(k,t)) break;
    }
    return false;
}

bool KingMove (bool colorLogic, Board& b, int i, int j, int& Rank, int& File) {

  if ((i-1 > -1) &&             ((!colorLogic && b.board_[i-1][j] == "♚")   || (colorLogic && b.board_[i-1][j] == "♔"))) {
    Rank = i-1; File = j; return true;
  }
  else if ((i-1 > -1) && (j-1 > -1) && ((!colorLogic && b.board_[i-1][j-1] == "♚") || (colorLogic && b.board_[i-1][j-1] == "♔"))) {
    Rank = i-1; File = j-1; return true;
  }
  else if ((j+1 < 8) &&              ((!colorLogic && b.board_[i][j+1] == "♚")   || (colorLogic && b.board_[i][j+1] == "♔"))) {
    Rank = i; File = j+1; return true;
  }
  else if ((i-1 > -1) && (j+1 < 8) &&  ((!colorLogic && b.board_[i-1][j+1] == "♚") || (colorLogic && b.board_[i-1][j+1] == "♔"))) {
    Rank = i-1; File = j+1; return true;
  }
  else if ((i+1 < 8) && (j-1 > -1) &&  ((!colorLogic && b.board_[i+1][j-1] == "♚") || (colorLogic && b.board_[i+1][j-1] == "♔"))) {
    Rank = i+1; File = j-1; return true;
  }
  else if ((i+1 < 8) &&              ((!colorLogic && b.board_[i+1][j] == "♚")   || (colorLogic && b.board_[i+1][j] == "♔"))) {
    Rank = i+1; File = j; return true;
  }
  else if ((j-1 > -1) &&             ((!colorLogic && b.board_[i][j-1] == "♚")   || (colorLogic && b.board_[i][j-1] == "♔"))) {
    Rank = i; File = j-1; return true;
  }
  else if ((i+1 < 8) && (j+1 < 8) &&   ((!colorLogic && b.board_[i+1][j+1] == "♚") || (colorLogic && b.board_[i+1][j+1] == "♔"))) {
    Rank = i+1; File = j+1; return true;
  }
  return false;
}
