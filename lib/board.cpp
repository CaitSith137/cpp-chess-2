#include "board.hpp"
#include "moves.hpp"

// Parse chess board notation (a to e, 1 to 8) into array notation (0 to 7, 7 to 0)
int Board::parse(const char input) {
  if ((int)input >= 97 && (int)input <= 104) {legalMove=true; return (int)input - 97;} // characters a to e
  else if ((int)input >= 49 && (int)input <= 56) {legalMove=true; return 56 - (int)input;} // characters 1 to 8
  else if (input == 'O' || input == '-') {legalMove=true; return -1;} // castling
  std::cout << "Illegal input!\n";
  legalMove = false; return -2;
}

bool Board::isEmpty(const int i, const int j) const {
  return board_[i][j] == "□" || board_[i][j] == "■";
}

bool Board::isWhite(const int i, const int j) const {
  assert(!isEmpty(i,j));
  return board_[i][j] == "♙" || board_[i][j] == "♖" || board_[i][j] == "♘" || board_[i][j] == "♗" || board_[i][j] == "♕" || board_[i][j] == "♔";
}

bool Board::isOpposite(const int i, const int j) const {
  assert(!isEmpty(i,j));
  return WhiteMove ? !isWhite(i,j) : isWhite(i,j);
}

void Board::init() {
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      if (i > 1 && i < 6)
	board_[i][j] = (i+j)%2==0 ? "□" : "■";
    }
  }

  for (int j = 0; j < 8; j++) {
    board_[1][j] = "♟";
    board_[6][j] = "♙";
  }

  board_[0][0] = "♜";  board_[0][7] = "♜";
  board_[0][1] = "♞";  board_[0][6] = "♞";
  board_[0][2] = "♝";  board_[0][5] = "♝";
  board_[0][3] = "♛";  board_[0][4] = "♚";

  board_[7][0] = "♖";  board_[7][7] = "♖";
  board_[7][1] = "♘";  board_[7][6] = "♘";
  board_[7][2] = "♗";  board_[7][5] = "♗";
  board_[7][3] = "♕";  board_[7][4] = "♔";
}

void Board::print() {
  std::sort(captureListBlack.begin(), captureListBlack.end());
  std::sort(captureListWhite.begin(), captureListWhite.end());
  std::reverse(captureListBlack.begin(), captureListBlack.end());
  std::reverse(captureListWhite.begin(), captureListWhite.end());
  
  for (unsigned int i = 0; i < captureListBlack.size(); i++) std::cout << captureListBlack.at(i);
  std::cout << std::endl;
    
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      std::cout << board_[i][j] << " ";
    }
    std::cout << std::endl;
  }

  for (unsigned int i = 0; i < captureListWhite.size(); i++) std::cout << captureListWhite.at(i);
  std::cout << std::endl;
}

bool Board::isPawnMove(const std::string input) const {
  return input[0] != 'N' && input[0] != 'K' && input[0] != 'Q' && input[0] != 'B' && input[0] != 'R' && input[0] != 'O';
}

void Board::move(std::string input) {

  if (input.size() < 2) {std::cout << "Illegal input!\n" << std::endl; legalMove = false; return;}

  int i = parse(input[input.size()-1]);  if (!legalMove) return;
  int j = parse(input[input.size()-2]);  if (!legalMove) return;

  // Pawn
  if (isPawnMove(input)) {
    tryPawnMove(i, j, input);
    goto End;
  }
  
  if (input.size() < 3) {std::cout << "Illegal input!\n"; legalMove = false; return;}

  // Ignore capture notation for non-pawn pieces
  if (input.find('x') != std::string::npos) input.erase(std::remove(input.begin(),input.end(),'x'));

  // Knight
  if (input[0] == 'N') {
    tryMove(i, j, Knight, input); goto End;
  }

  // Bishop
  else if (input[0] == 'B') {
    tryMove(i, j, Bishop, input); goto End;
  }

  // Rook
  else if (input[0] == 'R') {
    tryMove(i, j, Rook, input); goto End;
  }

  // Queen
  else if (input[0] == 'Q') {
    tryMove(i, j, Queen, input); goto End;
  }

  // King
  else if (input[0] == 'K') {
    tryMove(i, j, King, input);
    
    // Castling now impossible
    if (WhiteMove && legalMove) {WhiteKingside = false; WhiteQueenside = false;}
    else if (legalMove) {BlackKingside = false; BlackQueenside = false;}
    goto End;
  }

  // Castling
  else if (input[0] == 'O') {
    Castle(input); goto End;
  }
  
  legalMove = false; return;

 End:
  if (legalMove)
    if (WhiteMove) WhiteEnPassant = false;
    else BlackEnPassant = false;
  return;
}

// Color logic as follows: if it is white's move and we check whether white king is in discovered check,
// we need to invert isReachable() to look for opposing piece that can capture white king. Don't
// invert color if it is white's move and we check whether black king is in check.
// References i and j store the coords of potential attacker on king.
int Board::isCheck(int& i, int& j, const Board::Color color) {
  int KingRank, KingFile;
  bool colorLogic = (WhiteMove && color == White) || (!WhiteMove && color == Black);
  if (color == White) {KingRank = WhiteKingPos[0]; KingFile = WhiteKingPos[1];}
  else {KingRank = BlackKingPos[0]; KingFile = BlackKingPos[1];}
  return isReachable(KingRank, KingFile, i, j, All, colorLogic);
}

bool Board::KingLegalMoves() { // investigate test game move 30

  int i, j;
  if (WhiteMove) {i = BlackKingPos[0]; j = BlackKingPos[1];}
  else {i = WhiteKingPos[0]; j = WhiteKingPos[1];}
  
  if      (i+1 <  8 && j+1 <  8 && testMove(i+1, j+1, i, j, King)) return true;
  else if (j+1 <  8 && testMove(i, j+1, i, j, King)) return true;
  else if (i-1 > -1 && j+1 <  8 && testMove(i-1, j+1, i, j, King)) return true;
  else if (i-1 > -1 && testMove(i-1, j, i, j, King)) return true;
  else if (i-1 > -1 && j-1 > -1 && testMove(i-1, j-1, i, j, King)) return true;
  else if (j-1 > -1 && testMove(i, j-1, i, j, King)) return true;
  else if (i+1 <  8 && j-1 > -1 && testMove(i+1, j-1, i, j, King)) return true;
  else if (i+1 <  8 && testMove(i+1, j, i, j, King)) return true;
  std::cout << "King has no legal moves!\n";
  return false;
}

void Board::isCheckmate() {

  // 1. See if king has legal moves
  if (KingLegalMoves()) return;

  // 2. If no legal moves and double check: checkmate
  if ((WhiteMove && (BlackInCheck>1)) || (!WhiteMove && (WhiteInCheck>1))) {
    std::cout << "Double checkmate!\n"; Checkmate = true; return;
  }
  
  // 3. See if attacking piece can be captured (if yes, do another check detection)
  
  int AttackerRank, AttackerFile; // coords of attacker on king
  int i, j; // coords of potential counter-attacker

  if (!WhiteMove) {AttackerRank = altAttackerWhite[0]; AttackerFile = altAttackerWhite[1];}
  else {AttackerRank = altAttackerBlack[0]; AttackerFile = altAttackerBlack[1];}

  if (isReachable(AttackerRank, AttackerFile, i, j, All, true)) {//std::cout << AttackerRank << " " <<AttackerFile << " " <<i << " " << j << "testing\n";
    if (testMove(AttackerRank, AttackerFile, i, j, coords2Piece(i,j))) {
      std::cout << board_[AttackerRank][AttackerFile] << " can be captured by " << board_[i][j] << "\n"; return;}
  }

  // 4. If attacking piece not a line piece: checkmate
  Piece attackerPiece  = coords2Piece(AttackerRank, AttackerFile);

  if (attackerPiece != Rook && attackerPiece != Bishop && attackerPiece != Queen) {
    std::cout << "Checkmate!\n"; Checkmate = true; return;
  }
  
  // 5. See if attacking line piece can be blocked (if yes, do another check detection)
  // Iterate from king towards attacking piece and check if empty squares are reachable by friendly
  int KingRank, KingFile;
  if (WhiteMove) {KingRank = BlackKingPos[0]; KingFile = BlackKingPos[1];}
  else {KingRank = WhiteKingPos[0]; KingFile = WhiteKingPos[1];}

  if (isBlockable(AttackerRank, AttackerFile, KingRank, KingFile, i, j) && coords2Piece(i,j) != King) {
    std::cout << board_[AttackerRank][AttackerFile] << " can be blocked by " << board_[i][j] << "\n";
    return;
  }
  
  std::cout << "Checkmate!!!\n";
  Checkmate = true; return;
}

// Checks whether attack on (Rank,File) can be blocked, stores coords of potential blocker in i, j
bool Board::isBlockable(const int AttackerRank, const int AttackerFile, const int Rank, const int File, int& i, int& j) {

  int t = File;

  // Attacker in top left diagonal
  if (AttackerFile < File && AttackerRank < Rank) {
    for (int k = Rank-1; k > AttackerRank && --t > AttackerFile; k--) {//std::cout << "0looking in " << k << " " << t << "\n";
      if (isReachable(k, t, i, j, All, !WhiteMove)) {
	Piece piece = coords2Piece(i,j);
	if (testMove(k, t, i, j, piece)) return true;
      }
    }
  } t = File;
  // Top right diagonal
  if (AttackerFile > File && AttackerRank < Rank) {
    for (int k = Rank-1; k > AttackerRank && ++t < AttackerFile; k--) {//std::cout << "1looking in " << k << " " << t << "\n";
      if (isReachable(k, t, i, j, All, !WhiteMove)) {
	Piece piece = coords2Piece(i,j);
	if (testMove(k, t, i, j, piece)) return true;
      }
    }
  } t = File;
  // Bottom right diagonal
  if (AttackerFile > File && Rank < AttackerRank) {
    for (int k = Rank+1; k < AttackerRank && ++t < AttackerFile; k++) {//std::cout << "2looking in " << k << " " << t << "\n";
      if (isReachable(k, t, i, j, All, !WhiteMove)) {
	Piece piece = coords2Piece(i,j);
	if (testMove(k, t, i, j, piece)) return true;
      }
    }
  } t = File;
  // Bottom left diagonal
  if (AttackerFile < File && Rank < AttackerRank) {
    for (int k = Rank+1; k < AttackerRank && AttackerFile < --t; k++) {//std::cout << "3looking in " << k << " " << t << "\n";
      if (isReachable(k, t, i, j, All, !WhiteMove)) {
	Piece piece = coords2Piece(i,j);
	if (testMove(k, t, i, j, piece)) return true;
      }
    }
  }
  // Top vertical
  if (Rank > AttackerRank && AttackerFile == File) {
    for (int k = Rank-1; k > AttackerRank; k--) {//std::cout << "4looking in " << k << " " << File << "\n";
      if (isReachable(k, File, i, j, All, !WhiteMove)) {
	Piece piece = coords2Piece(i,j);
	if (testMove(k, File, i, j, piece)) return true;
      }
    }
  }
  // Bottom vertical
  else if (Rank < AttackerRank && AttackerFile == File) {
    for (int k = Rank+1; k < AttackerRank; k++) {//std::cout << "5looking in " << k << " " << File << "\n";
      if (isReachable(k, File, i, j, All, !WhiteMove)) {
	Piece piece = coords2Piece(i,j);
	if (testMove(k, File, i, j, piece)) return true;
      }
    }
  }
  // Right horizontal
  else if (File < AttackerFile && AttackerRank == Rank) {
    for (int k = File+1; k < AttackerFile; k++) {//std::cout << "6looking in " << Rank << " " << k << "\n";
      if (isReachable(Rank, k, i, j, All, !WhiteMove)) {
	Piece piece = coords2Piece(i,j);
	if (testMove(Rank, k, i, j, piece)) return true;
      }
    }
  }
  // Left horizontal
  else if (File > AttackerFile && AttackerRank == Rank) {
    for (int k = File-1; k > AttackerFile; k--) {//std::cout << "7looking in " << Rank << " " << k << "\n";
      if (isReachable(Rank, k, i, j, All, !WhiteMove)) {
	Piece piece = coords2Piece(i,j);
	if (testMove(Rank, k, i, j, piece)) return true;
      }
    }
  }
  
}

// TO DO: Add pawn regular move to isReachable(Piece=all)
void Board::isStalemate() {

  if (KingLegalMoves()) return;

  // Check if any other pieces can move by iterating over the entire board and calling isReachable on every empty/opposing square

  int Rank, File;
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      if (isEmpty(i,j) || !isOpposite(i,j)) {
	if (isReachable(i, j, Rank, File, All, true)) {
	    if (testMove(i, j, Rank, File, coords2Piece(Rank,File))) return;
	}
      }
    }
  }
  
  if (PieceCount() == 2) {std::cout << "Draw by insufficient material.\n"; Stalemate = true; return;}
  Stalemate = true; return;
}

void Board::tryPawnMove(const int i, const int j, const std::string input) {
  int Rank = -1;
  int File = -1;
  int StartFile = -1;
  int StartRank = -1;
  
  // Without capture
  if (input.size() == 2) {
    legalMove = isReachable(i, j, Rank, File, Pawn, false) > 0;
    if (!legalMove || !isEmpty(i,j)) {std::cout << "Illegal pawn move!\n"; legalMove = false; return;}
    doMove(i, j, Rank, j, Pawn);
    if (!WhiteMove && i-2==Rank) {
      EnPassantFile = j; WhiteEnPassant = true; std::cout << "White can en passant\n";
    }
    else if (WhiteMove && i+2==Rank) {
      EnPassantFile = j; BlackEnPassant = true; std::cout << "Black can en passant\n";
    }
  }
  // With capture
  else if (input.find('x') == 1 && input.size() == 4) {
    StartFile = parse(input[0]);
    StartRank = WhiteMove? i+1 : i-1;
    legalMove = isReachable(i, j, Rank, File, PawnCap, false) > 0;

    if (!legalMove) {std::cout << "Illegal pawn capture!\n"; return;}
    doMove(i, j, StartRank, StartFile, Pawn);
  }
  else {legalMove = false; std::cout << "Illegal pawn input!\n"; return;}

  int isBlackCheck = isCheck(attackerBlack[0], attackerBlack[1], Black);
  int isWhiteCheck = isCheck(attackerWhite[0], attackerWhite[1], White);

  legalMove = WhiteMove ? !(isWhiteCheck>0) : !(isBlackCheck>0);
  if (legalMove) {
    if (EnPassant) {
      if (WhiteMove) {
	board_[i+1][j] = (i+1+j)%2==0 ? "□" : "■";
	captureListWhite.push_back("♟");
      }
      else {
	board_[i-1][j] = (i-1+j)%2==0 ? "□" : "■";
	captureListBlack.push_back("♙");
      }
    }
    
    BlackInCheck = isBlackCheck;
    WhiteInCheck = isWhiteCheck;
    if ((!WhiteMove && WhiteInCheck) || (WhiteMove && BlackInCheck)) isCheckmate();
    else isStalemate();
  }
  else {
    std::cout << "Illegal move! (Self-check)\n";
    EnPassantFile = -1; EnPassant = false;
    if (input.size() == 2) (undoMove(i, j, Rank, j, Pawn));
    else (undoMove(i, j, StartRank, StartFile, Pawn));
    return;
  }

  // Promotion
 Promotion:
  if (legalMove && (i == 0 || i == 7)) {
    std::cout << "Promote pawn: Q, R, B, N" << std::endl;
    char piece;
    std::cin >> piece;
    switch(piece)
      {
      case 'Q': board_[i][j] = WhiteMove ? "♕" : "♛";  break;
      case 'R': board_[i][j] = WhiteMove ? "♖" : "♜";  break;
      case 'B': board_[i][j] = WhiteMove ? "♗" : "♝";  break;
      case 'N': board_[i][j] = WhiteMove ? "♘" : "♞";  break;
      default : std::cout << "Illegal promotion!\n"; goto Promotion;
      }
  }	  
}

void Board::tryMove(const int i, const int j, const Board::Piece piece, const std::string input) {
  int PieceX = -1;
  int PieceY = -1;
  
  if (input.size()>3)
    resolveAmbiguousMove(PieceY, PieceX, input, piece);
  else
    legalMove = isReachable(i, j, PieceY, PieceX, piece, false) > 0;
      
  if (legalMove && (isEmpty(i,j) || isOpposite(i,j))) {
    
    doMove(i, j, PieceY, PieceX, piece);
    if (piece == King) {
      if (WhiteMove) {WhiteKingPos[0] = i; WhiteKingPos[1] = j;}
      else {BlackKingPos[0] = i; BlackKingPos[1] = j;}
    }

    int isBlackCheck = isCheck(attackerBlack[0], attackerBlack[1], Black);
    int isWhiteCheck = isCheck(attackerWhite[0], attackerWhite[1], White);

    legalMove = WhiteMove ? !(isWhiteCheck) : !(isBlackCheck);
    if (legalMove) {
      BlackInCheck = isBlackCheck;
      WhiteInCheck = isWhiteCheck;
      if ((!WhiteMove && WhiteInCheck) || (WhiteMove && BlackInCheck)) isCheckmate();
      else isStalemate();
      return;
    }
    else {
      std::cout << "Illegal move! (Self-check)\n";
      undoMove(i, j, PieceY, PieceX, piece); 	
      return;
    }
  }
  else {
    std::cout << "Illegal " << piece << " move!\n";
    legalMove = false; return;
  }
}

// During own move test if opposing piece can move from (Rank, File) to (i, j)
// Example: during white turn, check if black king has legal moves.
// Need to invert logic: isOpposite, doMove, undoMove
bool Board::testMove(const int i, const int j, const int Rank, const int File, const Board::Piece piece) {
  bool legal = false;

  legal = coords2Piece(Rank, File) == piece;

  if (legal && (isEmpty(i,j) || !isOpposite(i,j))) {
 
    doMove(i, j, Rank, File, piece, true);
    if (piece == King) {
      if (!WhiteMove) {WhiteKingPos[0] = i; WhiteKingPos[1] = j;}
      else {BlackKingPos[0] = i; BlackKingPos[1] = j;}
    }
 
    int isBlackCheck = isCheck(altAttackerBlack[0], altAttackerBlack[1], Black);
    int isWhiteCheck = isCheck(altAttackerWhite[0], altAttackerWhite[1], White);

    legal = !WhiteMove ? !(isWhiteCheck) : !(isBlackCheck);
    
    undoMove(i, j, Rank, File, piece, true);      
    return legal;
  }
  else
    return false;
}

// TO DO: FIX EN PASSANT
void Board::doMove(const int i, const int j, const int k, const int l, const Board::Piece p, const bool inverted) {
  std::string piece;
  const bool colorLogic = inverted? !WhiteMove : WhiteMove;
  switch(p)
    {
    case King:   piece = colorLogic ? "♔" : "♚";  break;
    case Queen:  piece = colorLogic ? "♕" : "♛";  break;
    case Rook:   piece = colorLogic ? "♖" : "♜";  break;
    case Bishop: piece = colorLogic ? "♗" : "♝";  break;
    case Knight: piece = colorLogic ? "♘" : "♞";  break;
    case Pawn:   piece = colorLogic ? "♙" : "♟";  break;
    case All:    std::cout << "Cannot move all pieces!\n";
    }
  captured = isEmpty(i,j) ? "" : board_[i][j];
  if (!captured.empty() && WhiteMove) captureListWhite.push_back(board_[i][j]);
  else if (!captured.empty() && !WhiteMove) captureListBlack.push_back(board_[i][j]);
  board_[k][l] = (k+l)%2==0 ? "□" : "■"; board_[i][j] = piece;
}

void Board::undoMove(const int i, const int j, const int k, const int l, const Piece p, const bool inverted) {
  std::string piece;
  const bool colorLogic = inverted? !WhiteMove : WhiteMove;
  switch(p)
    {
    case King:   piece = colorLogic ? "♔" : "♚";  break;
    case Queen:  piece = colorLogic ? "♕" : "♛";  break;
    case Rook:   piece = colorLogic ? "♖" : "♜";  break;
    case Bishop: piece = colorLogic ? "♗" : "♝";  break;
    case Knight: piece = colorLogic ? "♘" : "♞";  break;
    case Pawn:   piece = colorLogic ? "♙" : "♟";  break;
    case All:    std::cout << "Cannot move all pieces!\n";
    }

  std::string old = captured.empty() ? ((i+j)%2==0 ? "□" : "■") : captured;
  board_[k][l] = piece; board_[i][j] = old;
  if (!captured.empty() && WhiteMove) captureListWhite.pop_back();
  else if (!captured.empty() && !WhiteMove) captureListBlack.pop_back();
  //std::cout << "undoing move " << old << " " << captured << "\n";
  if (p == King) {
    if (colorLogic) {WhiteKingPos[0] = k; WhiteKingPos[1] = l;}
    else {BlackKingPos[0] = k; BlackKingPos[1] = l;}
  }
}

// If inverted, look whether  (i,j) can be reached by opposing piece at (Rank,File). Else look for friendly piece.
int Board::isReachable(const int i, const int j, int& Rank, int& File, const Board::Piece type, bool inverted, bool blockmode) {

  bool colorLogic = inverted ? !WhiteMove : WhiteMove;
  int count = 0;
  
  if (type == All || type == Queen) {
    if (QueenMove(colorLogic, *this, i, j, Rank, File)) {count++; if (count > 1) goto end;}
  }
  
  if (type == All || type == Bishop) {
    if (BishopMove(colorLogic, *this, i, j, Rank, File)) {count++; if (count > 1) goto end;}
  }
  
  if (type == All || type == Rook) {
    if (RookMove(colorLogic, *this, i, j, Rank, File)) {count++; if (count > 1) goto end;}
  }

  if (type == All || type == PawnCap) {
    if (PawnCapture(colorLogic, *this, i, j, Rank, File)) {count++; if (count > 1) goto end;}
  }
  
  if (type == Pawn) {
    if (PawnMove(colorLogic, *this, i, j, Rank, File)) {count++; if (count > 1) goto end;}
  }
  
  if (type == All || type == Knight) {
    if (KnightMove(colorLogic, *this, i, j, Rank, File)) {count++; if (count > 1) goto end;}
  }
  
  if (type == All || type == King) {
    if (KingMove(colorLogic, *this, i, j, Rank, File)) {count++; if (count > 1) goto end;}
  }
  
 end:
  return count;
}

void Board::resolveAmbiguousMove(int& Rank, int& File, const std::string input, const Piece p) {

  File = parse(input[1]); if (!legalMove) return;
  Rank = parse(input[2]); if (!legalMove) return;

  std::string piece;

  switch(p)
    {
    case Queen:  piece = WhiteMove ? "♕" : "♛";  break;
    case Rook:   piece = WhiteMove ? "♖" : "♜";  break;
    case Bishop: piece = WhiteMove ? "♗" : "♝";  break;
    case Knight: piece = WhiteMove ? "♘" : "♞";  break;
    }
    
  if ((WhiteMove && board_[Rank][File] != piece) || (!WhiteMove && board_[Rank][File] != piece)) {
    std::cout << "Illegal move!\n";
    legalMove = false; return;
  }
  
  legalMove = true;
}

void Board::Castle(std::string input) {

  // Kingside
  if (input.size() == 3) {
    if (WhiteMove && WhiteKingside && board_[7][5] == "□" && board_[7][6] == "■") {
      WhiteKingside = false; WhiteQueenside = false;
      board_[7][6] = "♔"; board_[7][5] = "♖"; board_[7][7] = "□"; board_[7][4] = "■";
      WhiteKingPos[0] = 7; WhiteKingPos[1] = 6;
      legalMove = true; return;
    }
    else if (!WhiteMove && BlackKingside && board_[0][6] == "□" && board_[0][5] == "■") {
      BlackKingside = false; BlackQueenside = false;
      board_[0][6] = "♚"; board_[0][5] = "♜"; board_[0][4] = "□"; board_[0][7] = "■";
      BlackKingPos[0] = 0; BlackKingPos[1] = 6;
      legalMove = true; return;
    }
    else {std::cout << "Illegal castle!\n"; legalMove = false; return;}
  }
  
  // Queenside
  else if (input.size() == 5) {
    if (WhiteMove && WhiteQueenside && board_[7][1] == "□" && board_[7][2] == "■" && board_[7][3] == "□") {
      WhiteQueenside = false; WhiteQueenside = false;
      board_[7][2] = "♔"; board_[7][3] = "♖"; board_[7][0] = "■"; board_[7][4] = "■";
      WhiteKingPos[0] = 7; WhiteKingPos[1] = 2;
      legalMove = true; return;
    }
    else if (!WhiteMove && BlackQueenside && board_[0][1] == "■" && board_[0][2] == "□" && board_[0][3] == "■") {
      BlackQueenside = false; BlackQueenside = false;
      board_[0][2] = "♚"; board_[0][3] = "♜"; board_[0][0] = "□"; board_[0][4] = "□";
      BlackKingPos[0] = 0; BlackKingPos[1] = 2;
      legalMove = true; return;
    }
    else {std::cout << "Illegal castle!!\n"; legalMove = false; return;}
  }
  else {std::cout << "Illegal castle!!!\n"; legalMove = false; return;}
}

Board::Piece Board::coords2Piece(const int i, const int j) {
  assert(!isEmpty(i,j));
  const std::string s = board_[i][j];
  if      (s == "♖" || s == "♜") return Rook;
  else if (s == "♗" || s == "♝") return Bishop;
  else if (s == "♕" || s == "♛") return Queen;
  else if (s == "♙" || s == "♟") return Pawn;
  else if (s == "♘" || s == "♞") return Knight;
  else return King;
}

int Board::PieceCount() {
  int count = 0;
  for (int i = 0; i < 8; i++)
    for (int j = 0; j < 8; j++)
      if (!isEmpty(i,j)) count++;
  return count;
}


std::ostream& operator<<(std::ostream& os, Board::Piece piece) {
  switch(piece)
    {
    case Board::Pawn: os << "Pawn"; break;
    case Board::PawnCap: os << "Pawn capture"; break;
    case Board::Knight: os << "Knight"; break;
    case Board::Bishop: os << "Bishop"; break;
    case Board::Rook: os << "Rook"; break;
    case Board::Queen: os << "Queen"; break;
    case Board::King: os << "King"; break;
    case Board::All: os << "All"; break;
    }
  return os;
}
