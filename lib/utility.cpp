#include "utility.hpp"

void MoveList::PrintMoveList() {
  for (int i = 0; i < MoveList.size(); i++) {

    if (i%2==0) std::cout << (i%2==0 ? i/2 : (i+1)/2)+1 << ". ";
    
    std::cout << MoveList.at(i) << " ";

    if (i%2>0) std::cout << std::endl;
  }
}
