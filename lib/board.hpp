#ifndef BOARD_HPP
#define BOARD_HPP

#include <iostream>
#include <cassert>
#include <vector>
#include <string>
#include <algorithm>

class Board {
public:

  enum Piece {All, Pawn, PawnCap, Knight, Bishop, Rook, Queen, King};
  enum Color {White, Black};
  
  void init();
  void print();
  void move(std::string);
  void Castle(std::string);
  void tryPawnMove(const int, const int, const std::string);
  void tryMove(const int i, const int j, const Piece, const std::string);
  bool testMove(const int i, const int j, const int Rank, const int File, const Piece);
  void doMove(const int i, const int j, const int k, const int l, const Piece p, const bool inverted = false);
  void undoMove(const int i, const int j, const int k, const int l, const Piece p, const bool inverted = false);

  bool isPawnMove(const std::string) const;
  bool isEmpty(const int, const int) const;
  bool isWhite(const int, const int) const;
  bool isOpposite(const int, const int) const;
  int  isReachable(const int, const int, int&, int&, const Piece=All, const bool=false, const bool blockmode = false);
  int  isCheck(int&, int&, const Color);
  void isCheckmate();
  void isStalemate();
  bool KingLegalMoves();
  void resolveAmbiguousMove(int&, int&, const std::string, const Piece);
    
  std::string board_[8][8];
  bool WhiteMove;
  bool legalMove = true;
  int WhiteInCheck = false;
  int BlackInCheck = false;
  bool Checkmate = false;
  bool Stalemate = false;

  // En passant
  bool WhiteEnPassant = false;
  bool BlackEnPassant = false;
  bool EnPassant = false;
  int EnPassantFile = -1;

  // Castling
  bool BlackKingside = true;
  bool WhiteKingside = true;
  bool BlackQueenside = true;
  bool WhiteQueenside = true;
  
  std::vector<std::string> captureListWhite;
  std::vector<std::string> captureListBlack;
  
private:
  
  int parse(const char);
  int PieceCount();
  Piece coords2Piece(const int i, const int j);
  bool isBlockable(const int AttackerRank, const int AttackerFile, const int Rank, const int File, int& i, int& j);
  std::string captured;
  int WhiteKingPos[2] = {7,4};
  int BlackKingPos[2] = {0,4};
  int attackerBlack[2] = {-1,-1};
  int attackerWhite[2] = {-1,-1};
  int altAttackerBlack[2] = {-1,-1};
  int altAttackerWhite[2] = {-1,-1};
};

std::ostream& operator<<(std::ostream& os, Board::Piece piece);

#endif
